import { Provider } from './provider/provider';
import { Message } from './message';
import { AttemptStatus } from './messageObject';
export declare function getNextProvider(message: Message): Provider;
export declare function updateMessageDelivery(providerName: string, status: AttemptStatus): void;
