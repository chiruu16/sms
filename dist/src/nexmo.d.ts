/// <reference types="express" />
import { Provider } from "./provider";
import { Message } from "./message";
import * as express from 'express';
export declare class Nexmo extends Provider {
    YOUR_API_KEY: string;
    YOUR_API_SECRET: string;
    YOUR_VIRTUAL_NUMBER: string;
    Nexmo1: any;
    nexmo: any;
    constructor();
    protected onStateChange(): void;
    protected onSend(message: Message): void;
    protected registeredEndpoint(path: string, handler: (req: express.Request, res: express.Response) => void): void;
}
