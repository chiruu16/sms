"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const attempt_1 = require("./attempt");
class Message {
    constructor(json) {
        this.id = json.id;
        this.toTel = json.toTel;
        this.senderName = json.senderName;
        this.body = json.body;
        this.ttl = json.ttl;
        this.startTime = Date.now();
        this.attempts = json.attempts;
        this.priority = json.priority;
    }
    static get(json) {
        let message = Message.messages.get(json.id);
        if (message != null) {
            return message;
        }
        else {
            let message = new Message(json);
            Message.messages.set(message.id, message);
            return message;
        }
    }
    send() {
        const attempt = this.createAttempt();
        attempt.send();
    }
    createAttempt() {
        let curAttempt = new attempt_1.Attempt(this);
        if (!Array.isArray(this.attempts)) {
            this.attempts = [];
        }
        this.attempts.push(curAttempt);
        return curAttempt;
    }
    static getLastAttempt(message) {
        return message.attempts[message.attempts.length - 1];
    }
    static resendMessage(attempt) {
        let resendMessage = Message.messages.get(attempt.messageID);
        if (resendMessage != undefined) {
            resendMessage.send();
        }
        else {
            console.log("Error Resending - Failed Message ID not in Map");
        }
    }
    static deleteFromMessageMap(id) {
        Message.messages.delete(id);
    }
    //RESENDING MECHANISM ?
    static checkUnsentMessagePeriodically(RESEND_TIME_MS) {
        for (let [key, value] of Message.messages) {
            let currentTime = Date.now();
            //DELETE IF TTL EXPIRE
            if (value.startTime + value.ttl > currentTime) {
                Message.deleteFromMessageMap(key);
                continue;
            }
            //IF ERROR IN ATTEMPTS = > HYPOTHETICAL => somehow provider sending the sms fails
            let lastAttemp = Message.getLastAttempt(value);
            if (lastAttemp.provider == undefined || lastAttemp.providerRef == undefined) {
                value.send();
            }
            //RETRY MESSAGE AFTER FIXED TIME.
            if (value.startTime + RESEND_TIME_MS > currentTime && (lastAttemp.status != "delivered")) {
                value.send();
            }
        }
    }
}
Message.messages = new Map();
exports.Message = Message;
//# sourceMappingURL=message.js.map