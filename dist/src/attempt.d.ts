import { AttemptStatus } from "./messageObject";
import { Provider } from "./provider/provider";
import { Message } from "./message";
export declare class Attempt {
    private message;
    static attempts: Map<string, Attempt>;
    status: AttemptStatus;
    provider: Provider;
    providerRef: string;
    messageID: string;
    constructor(message: Message);
    private setProvider(message);
    send(): void;
    static setProviderReference(attempt: Attempt, reference: string): void;
    static updateStatus(id: string, status: AttemptStatus): void;
}
