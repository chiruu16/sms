"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// not used right now
/*
export enum MessageStatus {
 sent = "sent",
 delivered = "delieverd",
 failed = "failed",
 queued = "queued",
}
*/
var MessagePriority;
(function (MessagePriority) {
    MessagePriority["none"] = "none";
    MessagePriority["low"] = "low";
    MessagePriority["normal"] = "normal";
    MessagePriority["high"] = "high";
})(MessagePriority = exports.MessagePriority || (exports.MessagePriority = {}));
var AttemptStatus;
(function (AttemptStatus) {
    AttemptStatus["failed"] = "failed";
    AttemptStatus["created"] = "created";
    AttemptStatus["sent"] = "sent";
    AttemptStatus["delieverd"] = "delivered";
})(AttemptStatus = exports.AttemptStatus || (exports.AttemptStatus = {}));
//# sourceMappingURL=messageObject.js.map