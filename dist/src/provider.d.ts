/// <reference types="express" />
import { Message } from "./message";
import { AttemptStatus } from "./messageObject";
import * as express from 'express';
export declare abstract class Provider {
    send(message: Message): void;
    protected onStateChange(reference: string, status: AttemptStatus): void;
    protected abstract onSend(message: Message): any;
    protected registeredEndpoint(path: string, handler: (req: express.Request, res: express.Response) => void): void;
}
