"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const message_1 = require("./message");
const attempt_1 = require("./attempt");
const http = require("http");
const bodyParser = require("body-parser");
const express = require("express");
const logger_1 = require("logger");
let logger = new logger_1.Logger("main");
const app = express();
//TODO =>
app.use(bodyParser.json()); // without the body parser req.body not read in Twilio
app.use(bodyParser.urlencoded({ extended: true }));
// SEND SMS from JSON
/*
var json = require('./messages.json');
for (let m in json) {
 //Message.get(json[m]).send();
 setTimeout(function () {
  Message.get(json[m]).send();
 }, 3000);
}
*/
// Periodically check the messages MAP
//let RESEND_TIME_MS = 180000; //3 minutes
//let TIME_INTERVAL_CHECK_ATTEMPT_MAP = 60000;
//setInterval(Message.checkUnsentMessagePeriodically(RESEND_TIME_MS), TIME_INTERVAL_CHECK_ATTEMPT_MAP);
//Set endpoint for incoming messages
app.post('/Message/IncomingRequest', (req, res) => {
    message_1.Message.get(req.body).send();
    logger.info(req.url, req.body);
});
app.post('/MessageStatus/Twilio', (req, res) => {
    attempt_1.Attempt.updateStatus(req.body.SmsSid, req.body.SmsStatus);
});
app.post('/MessageStatus/Nexmo', (req, res) => {
    attempt_1.Attempt.updateStatus(req.body.messageId, req.body.status);
});
let server = http.createServer(app);
server.listen(8080, () => {
    console.log('Messaging Server listening on port 8080!');
});
//# sourceMappingURL=main.js.map