import { MessageObject } from './messageObject';
import { Attempt } from './attempt';
export declare class Message {
    static messages: Map<string, Message>;
    id: string;
    toTel: string;
    private senderName;
    body: string;
    private ttl;
    private startTime;
    attempts: Attempt[];
    private priority;
    constructor(json: MessageObject);
    static get(json: MessageObject): Message;
    send(): void;
    private createAttempt();
    static getLastAttempt(message: Message): Attempt;
    static resendMessage(attempt: Attempt): void;
    static deleteFromMessageMap(id: string): void;
    static checkUnsentMessagePeriodically(RESEND_TIME_MS: number): void;
}
