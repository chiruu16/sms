"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const twilio_1 = require("./provider/twilio");
const logger_1 = require("logger");
let logger = new logger_1.Logger("Providers");
//PROVIDERS ARRAY - ADD NEW PROVIDERS
let providers = [];
providers.push(new twilio_1.Twilio("Twilio"));
//providers.push(new Nexmo("Nexmo"));
function getNextProvider(message) {
    //SIMPLE PROVIDER SWITCHING MECHANISM
    //let selectedProvider = providers[0];
    //providers.shift()
    //providers.push(selectedProvider);
    let selectedProvider = selectProviderBasedOnLastAttempt(message);
    selectedProvider.incrementMessageCount();
    return selectedProvider;
}
exports.getNextProvider = getNextProvider;
//Selecting the Provider based on the Last Attempt.
function selectProviderBasedOnLastAttempt(message) {
    if (message.attempts.length == 0) {
        return providers[0];
    }
    else {
        let prevProvider = message.attempts[message.attempts.length - 1].provider;
        for (let index in providers) {
            let selectedProvider = providers[index];
            if (prevProvider != selectedProvider) {
                return selectedProvider;
            }
        }
        return providers[0];
    }
}
function updateMessageDelivery(providerName, status) {
    for (let index in providers) {
        let currentProvider = providers[index];
        if (currentProvider.providerName == providerName) {
            if (status == "delivered") {
                currentProvider.incrementSuccessMessages();
            }
            else if (status == "failed") {
                currentProvider.incrementFailedMessages();
            }
            let output = "Provider: " + currentProvider.providerName + " Sent: " + currentProvider.messageCount + " Success: " + currentProvider.successMessages + " Failed: " + currentProvider.failedMessages + " MessageCost: " + currentProvider.messageCostUSD;
            logger.info(output);
            //console.log("Provider: " + currentProvider.providerName + " Sent: " + currentProvider.messageCount + " Success: " + currentProvider.successMessages + " Failed: " + currentProvider.failedMessages + " MessageCost: " + currentProvider.messageCostUSD);
        }
    }
}
exports.updateMessageDelivery = updateMessageDelivery;
//TODO - > SELECT PROVIDER BASED ON ANALYTICS
function selectBestProvider() {
}
//TODO =>
//things to note selcting a provider
// 1. Priority Set in the message
// 2. Past Success Failure
// 3. Cost
//# sourceMappingURL=providers.js.map