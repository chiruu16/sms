import { Provider } from "./provider";
import { Message } from "../message";
export declare class Nexmo extends Provider {
    providerName: string;
    readonly messageCostUSD: number;
    constructor(name: string);
    YOUR_API_KEY: string;
    YOUR_API_SECRET: string;
    YOUR_VIRTUAL_NUMBER: string;
    Nexmo1: any;
    nexmo: any;
    protected onSend(message: Message): void;
}
