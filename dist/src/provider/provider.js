"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Provider {
    constructor(name) {
        this.messageCostUSD = 0;
        this.providerName = name;
        this.messageCount = 0;
        this.successMessages = 0;
        this.failedMessages = 0;
    }
    send(message) {
        this.onSend(message);
    }
    //TODO - > Embed all following methods two one mabe ?
    incrementMessageCount() {
        this.messageCount = this.messageCount + 1;
    }
    incrementSuccessMessages() {
        this.successMessages = this.successMessages + 1;
    }
    incrementFailedMessages() {
        this.failedMessages = this.failedMessages + 1;
    }
}
exports.Provider = Provider;
//# sourceMappingURL=provider.js.map