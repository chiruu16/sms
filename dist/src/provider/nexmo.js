"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const provider_1 = require("./provider");
const message_1 = require("../message");
const attempt_1 = require("../attempt");
class Nexmo extends provider_1.Provider {
    constructor(name) {
        super(name);
        this.messageCostUSD = 0.062;
        //Credentials for the oDoc Nexmo Account.
        //€0.0510 price to SL
        this.YOUR_API_KEY = "d20bef22";
        this.YOUR_API_SECRET = "fbd81e897631b1f7";
        this.YOUR_VIRTUAL_NUMBER = "NEXMO";
        this.Nexmo1 = require('nexmo');
        this.nexmo = new this.Nexmo1({
            apiKey: this.YOUR_API_KEY,
            apiSecret: this.YOUR_API_SECRET
        });
    }
    onSend(message) {
        this.nexmo.message.sendSms(this.YOUR_VIRTUAL_NUMBER, message.toTel, message.body, (err, response) => {
            if (err) {
                console.log(err + "Nexmo settign the response");
            }
            else {
                console.log(response.messages[0]["message-id"]);
                attempt_1.Attempt.setProviderReference(message_1.Message.getLastAttempt(message), response.messages[0]["message-id"]);
                //TODO  => CREDIT FOR PROVIDER OVER?
            }
        });
    }
}
exports.Nexmo = Nexmo;
//# sourceMappingURL=nexmo.js.map