import { Message } from "../message";
export declare abstract class Provider {
    providerName: string;
    messageCount: number;
    successMessages: number;
    failedMessages: number;
    readonly messageCostUSD: number;
    constructor(name: string);
    send(message: Message): void;
    protected abstract onSend(message: Message): any;
    incrementMessageCount(): void;
    incrementSuccessMessages(): void;
    incrementFailedMessages(): void;
}
