import { Provider } from "./provider";
import { Message } from "../message";
export declare class Twilio extends Provider {
    readonly messageCostUSD: number;
    constructor(name: string);
    accountSid: string;
    authToken: string;
    client: any;
    protected onSend(message: Message): void;
}
