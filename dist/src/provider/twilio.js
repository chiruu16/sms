"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//import { twilio } from 'twilio'
const provider_1 = require("./provider");
const message_1 = require("../message");
const attempt_1 = require("../attempt");
class Twilio extends provider_1.Provider {
    constructor(name) {
        super(name);
        this.messageCostUSD = 0.06;
        //Buddhishan Trial Twilio Account credentials
        //$0.06 price to SL
        this.accountSid = 'ACb76c2b52a780c992bb10e8b617c8bd54';
        this.authToken = '258e24a82d1a459bbbd27f4b207c7d0b';
        //TODO - require thing ??
        this.client = require('twilio')(this.accountSid, this.authToken);
    }
    onSend(message) {
        this.client.messages
            .create({
            to: message.toTel,
            from: '+18562428763',
            body: message.body,
            statusCallback: 'https://a0d4bf17.ngrok.io/MessageStatus/Twilio',
        })
            .then((data) => {
            attempt_1.Attempt.setProviderReference(message_1.Message.getLastAttempt(message), data.sid);
        });
    }
}
exports.Twilio = Twilio;
//# sourceMappingURL=twilio.js.map