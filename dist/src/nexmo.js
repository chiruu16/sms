"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const provider_1 = require("./provider");
class Nexmo extends provider_1.Provider {
    constructor() {
        super();
        this.YOUR_API_KEY = "2ffb961a";
        this.YOUR_API_SECRET = "fd222d4861b87697";
        this.YOUR_VIRTUAL_NUMBER = "NEXMO";
        this.Nexmo1 = require('nexmo');
        this.nexmo = new this.Nexmo1({
            apiKey: this.YOUR_API_KEY,
            apiSecret: this.YOUR_API_SECRET
        });
        this.registeredEndpoint('callback/twilio', (req, res) => {
            // TODO extract the status change from req
            this.onStateChange();
        });
    }
    onStateChange() {
    }
    onSend(message) {
        this.nexmo.message.sendSms(this.YOUR_VIRTUAL_NUMBER, message.toTel, message.body);
    }
    registeredEndpoint(path, handler) {
    }
}
exports.Nexmo = Nexmo;
//# sourceMappingURL=nexmo.js.map