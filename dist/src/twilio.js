"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//import { twilio } from 'twilio'
const provider_1 = require("./provider");
const message_1 = require("./message");
const attempt_1 = require("./attempt");
const express = require("express");
const app = express();
class Twilio extends provider_1.Provider {
    constructor() {
        super();
        this.accountSid = 'AC8149fe35232128b150710fbbb19bfad7';
        this.authToken = '477a89adf0a4379c259323c764c473b6';
        //twilio = require('twilio');
        //client = new this.twilio(this.accountSid, this.authToken);
        //TODO - require thing
        this.client = require('twilio')(this.accountSid, this.authToken);
        this.registeredEndpoint('http://9bfed9ce.ngrok.io/MessageStatus/Twilio', (req, res) => {
            // TODO extract the status change from req
            this.onStateChange(req.body.SmsSid, req.body.SmsStatus);
        });
    }
    //failed ,created,sent ,delieverd
    onStateChange(reference, status) {
        this.attempt.updateStatus(reference, status);
    }
    onSend(message) {
        this.client.messages
            .create({
            to: message.toTel,
            from: '+13142829594',
            body: message.body,
            statusCallback: 'http://9bfed9ce.ngrok.io/MessageStatus/Twilio',
        })
            .then((data) => {
            // this.providerRef = message.sid
            // TODO message1.getLastAttemp().setProviderRef(data.sid);
            attempt_1.Attempt.setProviderReference(message_1.Message.getLastAttempt(message), data.sid);
        });
    }
    static registeredEndpoint(path, handler) {
        app.post(path, handler => {
            console.log("sasad");
        });
    }
}
exports.Twilio = Twilio;
//# sourceMappingURL=twilio.js.map