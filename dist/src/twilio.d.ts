/// <reference types="express" />
import { Provider } from "./provider";
import { Message } from "./message";
import * as express from 'express';
import { AttemptStatus } from "./messageObject";
export declare class Twilio extends Provider {
    private attempt;
    accountSid: string;
    authToken: string;
    client: any;
    constructor();
    protected onStateChange(reference: string, status: AttemptStatus): void;
    providerRef: string;
    protected onSend(message: Message): void;
    protected static registeredEndpoint(path: string, handler: (req: express.Request, res: express.Response) => void): void;
}
