import { Attempt } from "./attempt";
export interface MessageObject {
    id: string;
    toTel: string;
    senderName: string;
    body: string;
    ttl: number;
    attempts: Attempt[];
    priority: MessagePriority;
}
export declare enum MessagePriority {
    none = "none",
    low = "low",
    normal = "normal",
    high = "high",
}
export declare enum AttemptStatus {
    failed = "failed",
    created = "created",
    sent = "sent",
    delieverd = "delivered",
}
