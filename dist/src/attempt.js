"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const messageObject_1 = require("./messageObject");
const message_1 = require("./message");
const providers_1 = require("./providers");
const logger_1 = require("logger");
let logger = new logger_1.Logger("Attempts");
class Attempt {
    constructor(message) {
        this.message = message;
        this.messageID = message.id;
        this.setProvider(message);
    }
    setProvider(message) {
        this.provider = providers_1.getNextProvider(message);
    }
    send() {
        this.provider.send(this.message);
    }
    //set provider reference & "add to the map"
    static setProviderReference(attempt, reference) {
        attempt.providerRef = reference;
        Attempt.attempts.set(reference, attempt);
    }
    //Called when the webhook / delivery status is received.
    static updateStatus(id, status) {
        let curAttempt = Attempt.attempts.get(id);
        if (curAttempt != null) {
            curAttempt.status = status;
            providers_1.updateMessageDelivery(curAttempt.provider.providerName, status);
            //console.log("MessageID:" + curAttempt.messageID + "  Provider: " + curAttempt.provider.providerName + "  ProviderID:" + curAttempt.providerRef + "  Status:" + curAttempt.status);
            let output = "MsgID:" + curAttempt.messageID + "  Prov: " + curAttempt.provider.providerName + "  ProvID:" + curAttempt.providerRef + "  Status:" + curAttempt.status;
            logger.info(output);
            // RESEND IF FAILED
            if (curAttempt.status == messageObject_1.AttemptStatus.failed) {
                console.log("Failed --> " + curAttempt.messageID); // How to test this ??
                message_1.Message.resendMessage(curAttempt);
            }
            // DELETE ATTEMPS FROM ATTEMT/MESSAGE MAP
            //if (curAttempt.status == AttemptStatus.delieverd) {
            //Attempt.attempts.delete(id);
            //Message.deleteFromMessageMap(curAttempt.messageID);
            //}
        }
        else {
            console.log("Error : Attempt not in map");
        }
    }
}
Attempt.attempts = new Map();
exports.Attempt = Attempt;
//# sourceMappingURL=attempt.js.map