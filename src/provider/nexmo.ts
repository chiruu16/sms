import { Provider } from "./provider";
import { Message } from "../message";
import { Attempt } from "../attempt";
import * as express from 'express';

export class Nexmo extends Provider {

 public providerName: string;
 public readonly messageCostUSD: number = 0.062;

 constructor(name: string) {
  super(name);
 }

 //Credentials for the oDoc Nexmo Account.
 //€0.0510 price to SL
 YOUR_API_KEY = "d20bef22";
 YOUR_API_SECRET = "fbd81e897631b1f7";
 YOUR_VIRTUAL_NUMBER = "NEXMO";

 Nexmo1 = require('nexmo');
 nexmo = new this.Nexmo1({
  apiKey: this.YOUR_API_KEY,
  apiSecret: this.YOUR_API_SECRET
 });

 protected onSend(message: Message) {
  this.nexmo.message.sendSms(this.YOUR_VIRTUAL_NUMBER, message.toTel, message.body,
   (err: any, response: any) => {
    if (err) {
     console.log(err + "Nexmo settign the response");
    } else {
     console.log(response.messages[0]["message-id"]);
     Attempt.setProviderReference(Message.getLastAttempt(message), response.messages[0]["message-id"]);
     //TODO  => CREDIT FOR PROVIDER OVER?
    }
   });
 }
}


