//import { twilio } from 'twilio'
import { Provider } from "./provider";
import { Message } from "../message";
import { Attempt } from "../attempt";
import * as express from 'express';

export class Twilio extends Provider {

  public readonly messageCostUSD: number = 0.06;

  constructor(name: string) {
    super(name);
  }

  //Buddhishan Trial Twilio Account credentials
  //$0.06 price to SL
  accountSid = 'ACb76c2b52a780c992bb10e8b617c8bd54';
  authToken = '258e24a82d1a459bbbd27f4b207c7d0b';

  //TODO - require thing ??
  client = require('twilio')(this.accountSid, this.authToken);

  protected onSend(message: Message) {
    this.client.messages
      .create({
        to: message.toTel,
        from: '+18562428763',
        body: message.body,
        statusCallback: 'https://a0d4bf17.ngrok.io/MessageStatus/Twilio', // => set the callback URL
      })
      .then((data: any) => { //if no data ???
        Attempt.setProviderReference(Message.getLastAttempt(message), data.sid);
      });
  }
}