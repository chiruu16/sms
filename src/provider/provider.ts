import { Message } from "../message";
import { AttemptStatus } from "../messageObject";
import * as express from 'express';

export abstract class Provider {

 public providerName: string;
 public messageCount: number;
 public successMessages: number;
 public failedMessages: number;

 public readonly messageCostUSD: number = 0;

 constructor(name: string) {
  this.providerName = name;
  this.messageCount = 0;
  this.successMessages = 0;
  this.failedMessages = 0;
 }
 public send(message: Message) {
  this.onSend(message);
 }

 protected abstract onSend(message: Message): any;

 //TODO - > Embed all following methods two one mabe ?
 public incrementMessageCount() {
  this.messageCount = this.messageCount + 1;
 }

 public incrementSuccessMessages() {
  this.successMessages = this.successMessages + 1;
 }

 public incrementFailedMessages() {
  this.failedMessages = this.failedMessages + 1;
 }

}



