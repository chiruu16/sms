import { AttemptStatus, MessageObject, MessagePriority } from './messageObject';
import { Message } from "./message";
import { Attempt } from "./attempt";
import * as http from 'http';
import * as bodyParser from 'body-parser'
import * as express from 'express';
import { Logger } from 'logger';

let logger = new Logger("main");
const app = express();

//TODO =>
app.use(bodyParser.json()); // without the body parser req.body not read in Twilio
app.use(bodyParser.urlencoded({ extended: true }));

// SEND SMS from JSON
/*
var json = require('./messages.json');
for (let m in json) {
 //Message.get(json[m]).send();
 setTimeout(function () {
  Message.get(json[m]).send();
 }, 3000);
}
*/

// Periodically check the messages MAP
//let RESEND_TIME_MS = 180000; //3 minutes
//let TIME_INTERVAL_CHECK_ATTEMPT_MAP = 60000;
//setInterval(Message.checkUnsentMessagePeriodically(RESEND_TIME_MS), TIME_INTERVAL_CHECK_ATTEMPT_MAP);

//Set endpoint for incoming messages
app.post('/Message/IncomingRequest', (req, res) => {
 Message.get(req.body).send();
 logger.info(req.url, req.body);
});

app.post('/MessageStatus/Twilio', (req, res) => {
 Attempt.updateStatus(req.body.SmsSid, req.body.SmsStatus);
});

app.post('/MessageStatus/Nexmo', (req, res) => {
 Attempt.updateStatus(req.body.messageId, req.body.status);
});

let server = http.createServer(app as any)
server.listen(8080, () => {
 console.log('Messaging Server listening on port 8080!');
});

