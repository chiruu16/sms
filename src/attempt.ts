import { AttemptStatus, MessageObject } from "./messageObject";
import { Provider } from "./provider/provider";
import { Message } from "./message";
import { getNextProvider, updateMessageDelivery } from "./providers";

import { Logger } from 'logger';
let logger = new Logger("Attempts");

export class Attempt {

 public static attempts = new Map<string, Attempt>();

 public status: AttemptStatus;
 public provider: Provider;
 public providerRef: string;
 public messageID: string;

 constructor(private message: Message) {
  this.messageID = message.id;
  this.setProvider(message);
 }

 private setProvider(message: Message) {
  this.provider = getNextProvider(message);
 }

 public send() {
  this.provider.send(this.message);
 }

 //set provider reference & "add to the map"
 static setProviderReference(attempt: Attempt, reference: string) {
  attempt.providerRef = reference;
  Attempt.attempts.set(reference, attempt);
 }

 //Called when the webhook / delivery status is received.
 static updateStatus(id: string, status: AttemptStatus) {
  let curAttempt = Attempt.attempts.get(id);
  if (curAttempt != null) {
   curAttempt.status = status;
   updateMessageDelivery(curAttempt.provider.providerName, status);
   //console.log("MessageID:" + curAttempt.messageID + "  Provider: " + curAttempt.provider.providerName + "  ProviderID:" + curAttempt.providerRef + "  Status:" + curAttempt.status);
   let output: string = "MsgID:" + curAttempt.messageID + "  Prov: " + curAttempt.provider.providerName + "  ProvID:" + curAttempt.providerRef + "  Status:" + curAttempt.status;
   logger.info(output);
   // RESEND IF FAILED
   if (curAttempt.status == AttemptStatus.failed) {
    console.log("Failed --> " + curAttempt.messageID);    // How to test this ??
    Message.resendMessage(curAttempt);
   }
   // DELETE ATTEMPS FROM ATTEMT/MESSAGE MAP
   //if (curAttempt.status == AttemptStatus.delieverd) {
   //Attempt.attempts.delete(id);
   //Message.deleteFromMessageMap(curAttempt.messageID);
   //}
  } else {
   console.log("Error : Attempt not in map")
  }
 }

}
