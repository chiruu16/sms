import { Attempt } from "./attempt";

export interface MessageObject {
 id: string;
 toTel: string;
 senderName: string;
 body: string;
 ttl: number;
 //startTime: Date;
 attempts: Attempt[]; // ??
 //status: MessageStatus;
 priority: MessagePriority;
}

// not used right now
/*
export enum MessageStatus {
 sent = "sent",
 delivered = "delieverd",
 failed = "failed",
 queued = "queued",
}
*/


export enum MessagePriority {
 none = "none",
 low = "low",
 normal = "normal",
 high = "high",
}

export enum AttemptStatus {
 failed = "failed",
 created = "created",
 sent = "sent",
 delieverd = "delivered",
}