import { Provider } from './provider/provider'
import { Twilio } from './provider/twilio'
import { Nexmo } from './provider/nexmo'
import { Message } from './message';
import { AttemptStatus } from './messageObject';

import { Logger } from 'logger';
let logger = new Logger("Providers");

//PROVIDERS ARRAY - ADD NEW PROVIDERS
let providers: Provider[] = [];
providers.push(new Twilio("Twilio"));
//providers.push(new Nexmo("Nexmo"));

export function getNextProvider(message: Message) {

 //SIMPLE PROVIDER SWITCHING MECHANISM
 //let selectedProvider = providers[0];
 //providers.shift()
 //providers.push(selectedProvider);

 let selectedProvider = selectProviderBasedOnLastAttempt(message);
 selectedProvider.incrementMessageCount();
 return selectedProvider;
}

//Selecting the Provider based on the Last Attempt.
function selectProviderBasedOnLastAttempt(message: Message) {
 if (message.attempts.length == 0) {
  return providers[0];
 } else {
  let prevProvider = message.attempts[message.attempts.length - 1].provider
  for (let index in providers) {
   let selectedProvider = providers[index];
   if (prevProvider != selectedProvider) {
    return selectedProvider;
   }
  }
  return providers[0];
 }
}

export function updateMessageDelivery(providerName: string, status: AttemptStatus) {
 for (let index in providers) {
  let currentProvider = providers[index];
  if (currentProvider.providerName == providerName) {
   if (status == "delivered") {
    currentProvider.incrementSuccessMessages();
   } else if (status == "failed") {
    currentProvider.incrementFailedMessages();
   }
   let output: string = "Provider: " + currentProvider.providerName + " Sent: " + currentProvider.messageCount + " Success: " + currentProvider.successMessages + " Failed: " + currentProvider.failedMessages + " MessageCost: " + currentProvider.messageCostUSD;
   logger.info(output);
   //console.log("Provider: " + currentProvider.providerName + " Sent: " + currentProvider.messageCount + " Success: " + currentProvider.successMessages + " Failed: " + currentProvider.failedMessages + " MessageCost: " + currentProvider.messageCostUSD);
  }
 }
}

//TODO - > SELECT PROVIDER BASED ON ANALYTICS
function selectBestProvider() {
}
//TODO =>
//things to note selcting a provider
// 1. Priority Set in the message
// 2. Past Success Failure
// 3. Cost
